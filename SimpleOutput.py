class SimpleOutput(object):

    def output(self, results):
        for result in results:
            print(result["name"] + " to " + result["host"], end="")
            print(" has succeded" if result["success"] else "has failed!")