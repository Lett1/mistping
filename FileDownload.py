import requests
import tempfile
import time

class FileDownload(object):

    def __init__(self):
        self.file_url = "http://ipv4.download.thinkbroadband.com/5MB.zip"

    def run(self):
        success = False
        error = None
        f = tempfile.TemporaryFile()
        start = time.clock()

        try:
            response = requests.get(self.file_url, stream=True, timeout=6)
            total_length = response.headers.get('content-length')

            if total_length is None:  # no content length header
                f.write(response.content)
            else:
                for data in response.iter_content(chunk_size=1024):
                    if data:
                        f.write(data)

            f.close()
            success = True

        except requests.exceptions.RequestException as e:    # This is the correct syntax
            error = e
            success = False

        return {"success": success, "host": self.file_url, "name": __name__, "error": error, "time": time.clock() - start}
