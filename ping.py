import datetime

from SimplePing import SimplePing
from FileDownload import FileDownload

from SimpleOutput import SimpleOutput
from PrettyPrintOutput import PrettyPrintOutput
from CSVOutput import CSVOutput

tests = [SimplePing("www.google.at"), FileDownload()]
outputs = [PrettyPrintOutput(), CSVOutput()]

results = []

for test in tests:
	result = {"starttime": datetime.datetime.now()}
	result.update(test.run())
	results.append(result)

for output in outputs:
	output.output(results)