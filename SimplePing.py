import os, platform


class SimplePing(object):

    def __init__(self, host="8.8.8.8"):
        self.host = host

    def run(self):
        # Simple ping class that test if a server is reachable by icmp ping
        # Uses the builtin system tool to do that

        ping_str = "-n 1" if platform.system().lower() == "windows" else "-c 1"

        # Ping
        success = os.system("ping " + ping_str + " " + self.host) == 0

        return {"success": success, "host": self.host, "name": __name__}
