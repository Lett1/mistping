from pprint import pprint


class PrettyPrintOutput(object):

    def output(self, results):
        for result in results:
            pprint(result)
            